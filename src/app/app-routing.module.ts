import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GameCreateComponent} from './content/game/create/game-create.component';
import {GameComponent} from './content/game/game.component';
import {LobbyComponent} from './content/game/lobby/lobby.component';
import {NameYourselfComponent} from './content/game/lobby/name-yourself/name-yourself.component';
import {ResultComponent} from './content/game/result/result.component';
import {LoginComponent} from './content/login/login.component';
import {RegisterComponent} from './content/register/register.component';
import {StartComponent} from './content/start/start.component';
import {LayoutComponent} from './layout/layout.component';
import {SessionStateEnum} from './shared/enums/SessionStateEnum';
import {AuthGuard} from './shared/guards/auth.guard';
import {SessionGuard} from './shared/guards/session.guard';

const routes: Routes = [
    {
      path: '',
      component: LayoutComponent,
      children: [
        {
          path: 'start', component: StartComponent
        },
        {
          path: 'login', component: LoginComponent
        },
        {
          path: 'register', component: RegisterComponent
        },
        {
          path: 'join/:sessionCode', component: NameYourselfComponent
        },
        {
          path: 'create',
          component: GameCreateComponent,
          canActivate: [AuthGuard]
        },
        {
          path: 'game', component: GameComponent, children: [

            {
              path: 'lobby', component: LobbyComponent, canActivate: [SessionGuard], data: {sessionState: SessionStateEnum.LOBBY}
            },
            {
              path: 'result', component: ResultComponent, canActivate: [SessionGuard], data: {sessionState: SessionStateEnum.GAME_ENDED}
            },
            {path: '**', redirectTo: 'start'}
          ]
        },
        {path: '**', redirectTo: 'start'}
      ]
    },
    {path: '**', redirectTo: 'start'}
  ]
;

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
