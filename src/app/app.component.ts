import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AuthService} from './shared/services/auth.service';
import {SessionService} from './shared/services/session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public loaded = false;

  constructor(
    private readonly translateService: TranslateService,
    private readonly authService: AuthService,
    private readonly sessionService: SessionService,
    private readonly router: Router
  ) {
    translateService.use('de');
    this.loaded = true;
    authService.restoreAuthentication();
  }
}
