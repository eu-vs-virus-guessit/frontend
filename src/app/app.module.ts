import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {StoreModule} from '@ngrx/store';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {environment} from '../environments/environment';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ContentModule} from './content/content.module';
import { NameYourselfComponent } from './content/game/lobby/name-yourself/name-yourself.component';
import {LayoutModule} from './layout/layout.module';
import {AuthGuard} from './shared/guards/auth.guard';
import {SessionGuard} from './shared/guards/session.guard';
import {AuthInterceptor} from './shared/interceptors/auth.interceptor';
import {TranslateInterceptor} from './shared/interceptors/translate.interceptor';
import {AuthService} from './shared/services/auth.service';
import {GameService} from './shared/services/game.service';
import {LanguageService} from './shared/services/language.service';
import {MiscService} from './shared/services/misc.service';
import {PlayerService} from './shared/services/player.service';
import {SessionService} from './shared/services/session.service';
import {authReducer} from './shared/store/reducers/auth/auth.reducer';
import {sessionReducer} from './shared/store/reducers/session/session.reducer';

export function createTranslateLoader(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, environment.domain + '/assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    NameYourselfComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ContentModule,
    RouterModule,
    LayoutModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    StoreModule.forRoot({
      auth: authReducer,
      session: sessionReducer
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TranslateInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    SessionService,
    SessionGuard,
    AuthService,
    GameService,
    LanguageService,
    AuthGuard,
    MiscService,
    PlayerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
