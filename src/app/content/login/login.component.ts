import {HttpErrorResponse} from '@angular/common/http';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.state';
import {AuthService} from '../../shared/services/auth.service';
import {LoginUser, LogoutUser} from '../../shared/store/actions/auth/auth.actions';
import {AuthResponse} from '../../shared/types/auth/AuthResponse';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginFormGroup: FormGroup;
  public error = false;
  private returnUrl = '';

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly store: Store<AppState>
  ) {
    this.loginFormGroup = formBuilder.group({
      username: new FormControl('', [Validators.required, Validators.minLength(1)]),
      password: new FormControl('', [Validators.required, Validators.minLength(1)]),
    });
  }

  public login(): void {
    this.loginFormGroup.updateValueAndValidity();
    if (this.loginFormGroup.valid) {
      const username = this.loginFormGroup.controls.username.value;
      const password = this.loginFormGroup.controls.password.value;
      this.authService.login(username, password).subscribe((auth) => {
        if (this.returnUrl) {
          this.router.navigateByUrl(this.returnUrl);
        } else {
          this.router.navigate(['start']);
        }
      }, (error: HttpErrorResponse) => {
        console.error(error);
        this.error = true;
      });
    }
  }

  public navigateToRegistration(): void {
    if(this.returnUrl) {
      this.router.navigate(
        ['register'],
        {
          queryParams: {
            returnUrl: this.returnUrl
          }
        });
    } else {
      this.router.navigate(['register']);
    }

  }

  public ngOnInit(): void {
    const returnUrl = this.route.snapshot.queryParams.returnUrl;
    if (returnUrl) {
      this.returnUrl = returnUrl;
    }
  }

}
