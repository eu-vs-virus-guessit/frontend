import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.state';
import {SessionStorageEnum} from '../../shared/enums/SessionStorageEnum';
import {AuthResponse} from '../../shared/types/auth/AuthResponse';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent implements OnDestroy {

  public error: string;

  public loggedIn = false;
  private storeSubscription;

  constructor(
    private readonly router: Router,
    private readonly store: Store<AppState>
  ) {
    this.storeSubscription = store.select('auth').subscribe((auth: AuthResponse) => {
      this.loggedIn = !!auth;
    });

    const currentNavigation = this.router.getCurrentNavigation();
    if(
      currentNavigation &&
      currentNavigation.extras &&
      currentNavigation.extras.state &&
      currentNavigation.extras.state.hasOwnProperty('error')) {
      this.error = currentNavigation.extras.state.error;
    }
  }

  public join(sessionId: string): void {
    if(sessionId) {
      this.router.navigate(['join', sessionId]);
    }
  }

  public ngOnDestroy(): void {
    this.storeSubscription.unsubscribe();
  }

}
