import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../shared/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public registrationFormGroup: FormGroup;
  private returnUrl: string = '';

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {
    this.registrationFormGroup = formBuilder.group({
      username: new FormControl('', [Validators.required, Validators.minLength(1)]),
      password: new FormControl('', [Validators.required, Validators.minLength(1)]),
      passwordRepeat: new FormControl('', [Validators.required, Validators.minLength(1)]),
      email: new FormControl('', [Validators.required, Validators.email]),
    });
  }

  public register(): void {
    if(this.registrationFormGroup.valid) {
      const username = this.registrationFormGroup.controls.username.value;
      const password = this.registrationFormGroup.controls.password.value;
      const passwordRepeat = this.registrationFormGroup.controls.passwordRepeat.value;
      const email = this.registrationFormGroup.controls.email.value;
      this.authService.register(username, password, passwordRepeat, email).subscribe((data) => {
        if (this.returnUrl) {
          this.router.navigateByUrl(this.returnUrl);
        } else {
          this.router.navigate(['start']);
        }
      }, () => {
        // TODO do something... maybe
      });
    }
  }

  public ngOnInit(): void {
    const returnUrl = this.route.snapshot.queryParams.returnUrl;
    if (returnUrl) {
      this.returnUrl = returnUrl;
    }
  }
}
