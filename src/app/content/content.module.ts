import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {ThirdPartyModule} from '../shared/modules/third-party/third-party.module';
import {GameCreateComponent} from './game/create/game-create.component';
import {GameComponent} from './game/game.component';
import {LobbyComponent} from './game/lobby/lobby.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {StartComponent} from './start/start.component';
import { ResultComponent } from './game/result/result.component';

@NgModule({
  declarations: [StartComponent, LobbyComponent, GameComponent, GameCreateComponent, LoginComponent, RegisterComponent, ResultComponent],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    ThirdPartyModule
  ],
  exports: [
  ]
})
export class ContentModule { }
