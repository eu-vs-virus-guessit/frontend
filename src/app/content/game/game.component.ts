import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavigationEnd, Router, RouterEvent} from '@angular/router';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.state';
import {fadeAnimation} from '../../shared/animations/router.animation';
import {SessionStateEnum} from '../../shared/enums/SessionStateEnum';
import {SessionStorageEnum} from '../../shared/enums/SessionStorageEnum';
import {MiscService} from '../../shared/services/misc.service';
import {Session} from '../../shared/types/session/Session';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
  animations: [fadeAnimation]
})
export class GameComponent implements OnInit, OnDestroy {

  private storeSubscription;
  private routerSubscription;

  constructor(
    private store: Store<AppState>,
    private readonly router: Router
  ) {
    this.routerSubscription = this.router.events.subscribe((event: RouterEvent) => {
      if(event instanceof NavigationEnd) {
        this.storeSubscription = this.store.select('session').subscribe((session: Session) => {
          if(session && MiscService.getUsernameFromJwt()) {
            switch (session.state) {
              case SessionStateEnum.LOBBY:
                this.router.navigate(['game', 'lobby']);
                break;
              case SessionStateEnum.GAME_IN_PROGRESS:
                this.router.navigate(['game', 'guess-it']);
                break;
              case SessionStateEnum.GAME_ENDED:
                this.router.navigate(['game', 'result']);
                break;
              default:
                // this should never happen
                this.router.navigate(['start']);
            }
          } else {
            this.router.navigate(['start']);
          }
        });
      }
    });
  }

  public ngOnDestroy(): void {
    this.storeSubscription.unsubscribe();
    this.routerSubscription.unsubscribe();
  }

  public ngOnInit(): void {

  }

}
