import {Component, OnDestroy} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../../app.state';
import {SessionService} from '../../../shared/services/session.service';
import {LeaveSession} from '../../../shared/store/actions/session/session.actions';
import {Player, createPlayer} from '../../../shared/types/game/Player';
import {Session} from '../../../shared/types/session/Session';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnDestroy {

  public session: Session;
  public slots: Player[];

  private sessionSub;

  constructor(
    private readonly store: Store<AppState>,
    private readonly sessionService: SessionService
  ) {

    this.sessionSub = store.select('session').subscribe((session: Session) => {
      if (session) {
        this.session = session;
        this.sessionService.listenToSessionActivity(session.code);
        this.slots = session.players.slice();
        while (this.slots.length < 8) { // replace with max players
          this.slots.push(createPlayer());
        }
      }
    });
  }

  public leaveLobby(): void {
    this.sessionService.leaveSession();
  }

  public ngOnDestroy(): void {
    this.sessionSub.unsubscribe();
  }

}
