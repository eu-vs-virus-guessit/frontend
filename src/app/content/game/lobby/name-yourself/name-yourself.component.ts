import {HttpErrorResponse} from '@angular/common/http';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {marker as _} from '@biesbjerg/ngx-translate-extract-marker';
import {MiscService} from '../../../../shared/services/misc.service';
import {SessionService} from '../../../../shared/services/session.service';

@Component({
  selector: 'app-name-yourself',
  templateUrl: './name-yourself.component.html',
  styleUrls: ['./name-yourself.component.scss']
})
export class NameYourselfComponent implements OnInit {

  public username: string;
  public sessionCode: string;

  public needUsername = false;

  public error: string;

  constructor(
    private sessionService: SessionService,
    private route: ActivatedRoute
  ) {
    route.params.subscribe((params: Params) => {
      if (params && params.sessionCode) {
        this.sessionCode = params.sessionCode;
        const username = MiscService.getUsernameFromJwt();
        if (username) {
          this.join(username);
        } else {
          this.needUsername = true;
        }
      }
    });
  }

  public join(username) {
    if (username && this.sessionCode) {
      this.sessionService.joinSession(username, this.sessionCode).subscribe({
        error: (error: HttpErrorResponse) => {
          console.error(error);
          this.error = _('Unable to join the session.');
        }
      });
    }
  }

  public ngOnInit(): void {
  }

}
