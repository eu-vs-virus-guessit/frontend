import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {marker as _} from '@biesbjerg/ngx-translate-extract-marker';
import {Store} from '@ngrx/store';
import {UriBuilder} from 'uribuilder';
import {environment} from '../../../../environments/environment';
import {AppState} from '../../../app.state';
import {SessionStorageEnum} from '../../../shared/enums/SessionStorageEnum';
import {moment} from '../../../shared/functions/Moment';
import {LanguageService} from '../../../shared/services/language.service';
import {MiscService} from '../../../shared/services/misc.service';
import {SessionService} from '../../../shared/services/session.service';
import {JoinSession} from '../../../shared/store/actions/session/session.actions';
import {AuthResponse} from '../../../shared/types/auth/AuthResponse';
import {Language} from '../../../shared/types/game/Language';
import {Session} from '../../../shared/types/session/Session';

@Component({
  selector: 'app-game-create',
  templateUrl: './game-create.component.html',
  styleUrls: ['./game-create.component.scss']
})
export class GameCreateComponent implements OnDestroy, OnInit {

  public languages: Language[];

  public creationFormGroup: FormGroup;

  private authResponseSub;

  constructor(
    private readonly router: Router,
    private readonly store: Store<AppState>,
    private readonly formBuilder: FormBuilder,
    private readonly languageService: LanguageService,
    private readonly sessionService: SessionService
  ) {
    this.authResponseSub = this.store.select('auth').subscribe((auth: AuthResponse) => {
      if(!auth) {
        this.router.navigate(['start'], {state: {error: _('You have to be logged in to create a game.')}});
      }
    });
  }

  public create(): void {
    if (this.creationFormGroup.valid) {
      const name = this.creationFormGroup.controls.name.value;
      const numberOfPlayers = this.creationFormGroup.controls.numberOfPlayers.value;
      const language = this.creationFormGroup.controls.language.value;
      this.sessionService.createSession(name, language, numberOfPlayers).subscribe((session: Session) => {
        this.store.dispatch(new JoinSession(session));
        this.router.navigate(['game', 'lobby']);
      });
    }
  }

  public ngOnDestroy(): void {
    this.authResponseSub.unsubscribe();
  }

  public ngOnInit(): void {
    this.creationFormGroup = this.formBuilder.group({
      name: new FormControl('', [Validators.required, Validators.minLength(1)]),
      numberOfPlayers: new FormControl(4),
      numberOfRounds: new FormControl(4),
      language: new FormControl('', [Validators.required, Validators.minLength(1)]),
    });

    this.languageService.getAvailableLanguages().subscribe((languages: Language[]) => {
      this.languages = languages;
    });
  }

}
