import {AuthResponse} from './shared/types/auth/AuthResponse';
import {Session} from './shared/types/session/Session';

export interface AppState {
  readonly auth: AuthResponse;
  readonly session: Session;
}
