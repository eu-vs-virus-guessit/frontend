import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {AppState} from '../../app.state';
import {AuthService} from '../../shared/services/auth.service';
import {AuthResponse} from '../../shared/types/auth/AuthResponse';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public env = environment;

  public authentication: Observable<AuthResponse>;

  constructor(
    private store: Store<AppState>,
    private authService: AuthService
  ) {
    this.authentication = store.select('auth');
    this.authentication.subscribe((data: AuthResponse) => {
    });
  }

  public logout() {
    this.authService.logout();
  }

  public ngOnInit(): void {
  }

}
