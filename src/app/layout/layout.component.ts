import { Component, OnInit } from '@angular/core';
import {fadeAnimation} from '../shared/animations/router.animation';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  animations: [fadeAnimation]
})
export class LayoutComponent implements OnInit {

  public ngOnInit(): void {
  }

}
