import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {finalize, tap} from 'rxjs/operators';

@Injectable()
export class TranslateInterceptor implements HttpInterceptor {

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // send the newly created request
    if (typeof req !== 'undefined') {
      return next.handle(req).pipe(
        finalize(() => {
        }),
        tap((ev: HttpEvent<any>) => {
          if (ev instanceof HttpResponse) {
            if (req.url.includes('i18n')) {
              const body = ev.body;
              Object.keys(body).forEach(key => {
                if (body[key].indexOf(['_[DEFAULT_VALUE]_']) !== -1) {
                  body[key] = body[key].replace('_[DEFAULT_VALUE]_', '');
                }
              });
              ev.clone({body});
            }
          }
        })) as any;
    } else {
      return next.handle(req);
    }
  }
}
