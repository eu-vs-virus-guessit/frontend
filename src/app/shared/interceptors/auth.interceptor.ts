import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {catchError} from 'rxjs/operators';

import {Observable, throwError} from 'rxjs';
import {SessionStorageEnum} from '../enums/SessionStorageEnum';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  public intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let authReq;
    const token = sessionStorage.getItem(SessionStorageEnum.ACCESS_TOKEN);
    // Füge allen 'nicht auth' Anfragen den Bearer Token hinzu
    if (token) {
      authReq = req.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    } else {
      authReq = req.clone();
    }

    // send the newly created request
    return next.handle(authReq).pipe(
      catchError((error, caught) =>
        throwError(error)));
  }
}
