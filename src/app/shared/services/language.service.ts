import { Injectable } from '@angular/core';
import { marker as _ } from '@biesbjerg/ngx-translate-extract-marker';
import {TranslateService} from '@ngx-translate/core';
import {Observable, of} from 'rxjs';
import {Language} from '../types/game/Language';

const AVAILABLE_LANGUAGES: Language[] = [
  {iso6391: 'de', name: _('german')},
  {iso6391: 'en', name: _('english')}
];

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  constructor(
    private readonly translateService: TranslateService
  ) {

  }

  public getAvailableLanguages(): Observable<Language[]> {
    return of(AVAILABLE_LANGUAGES);
  }

  /**
   * sets the current language to one of the supportet languages. It sets the default language otherwise
   * @param iso6391 - language iso6391 string
   */
  public setLanguage(iso6391: string): void {
    if (AVAILABLE_LANGUAGES.find(lang => lang.iso6391 === iso6391)) {
      this.translateService.use(iso6391);
    }
  }

  /**
   * translates a given string or string array
   * @param translateKey - the string or strings that shall be translated
   *
   * See documentation about the translation process:
   * https://confluence.2im.de/display/EPROC/Mehrsprachigkeit
   */
  public translate(translateKey: string | string[]): Observable<string | string[]> {
    return new Observable<string | string[]>((observer) => {
      this.translateService.get(translateKey).subscribe((translatedSequence) => {
        if (translateKey.length > 1) {
          observer.next(translatedSequence);
        } else {
          observer.next(translatedSequence[translateKey as string]);
        }
      });
    });
  }
}
