import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'webstomp-client';
import {Frame} from 'webstomp-client';
import {environment} from '../../../environments/environment';
import {AppState} from '../../app.state';
import {SessionStorageEnum} from '../enums/SessionStorageEnum';
import {moment} from '../functions/Moment';
import {
  JoinSession,
  LeaveSession,
  UpdateSession
} from '../store/actions/session/session.actions';
import {Language} from '../types/game/Language';
import {CreateSessionResponse} from '../types/session/CreateSessionResponse';
import {JoinSessionResponse} from '../types/session/JoinSessionResponse';
import {Session} from '../types/session/Session';
import {GameService} from './game.service';
import {MiscService} from './misc.service';
import {PlayerService} from './player.service';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  public private;

  private session: Session;

  private stompClient: Stomp.Client | null = null;
  private socketSubscription;

  constructor(
    private readonly store: Store<AppState>,
    private readonly router: Router,
    private readonly gameService: GameService,
    private readonly http: HttpClient
  ) {
    store.select('session').subscribe((session: Session) => {
      this.session = session;
    });
  }

  public createSession(name: string, language: Language, maxPlayers: number): Observable<Session> {
    return new Observable<Session>(observer => {
      const sessionCode = btoa(MiscService.encryptSha256(name + moment().unix().toString()));
      this.listenToSessionActivity(sessionCode).subscribe(() => {
        const url = `${environment.apiRoot}/register/session`;
        const body = {
          sessionCode,
          sessionName: name
        };

        return this.http.post(url, body).subscribe((res: CreateSessionResponse) => {
          PlayerService.setPlayerToStorate(res.player, res.session.code);
          observer.next(res.session);
        }, (error) => {
          observer.error(error);
        });
      }, (error: CloseEvent | Frame) => {
        observer.error(error);
      });
    });
  }

  public leaveSession(): void {
    this.stompClient.disconnect(() => {
      this.store.dispatch(new LeaveSession());
      sessionStorage.removeItem(SessionStorageEnum.SESSION);
      this.router.navigate(['start']);
    });
  }

  public listenToSessionActivity(sessionCode: string): Observable<void> {
    return new Observable(observer => {
      let url = `http://localhost:8080/websocket/tracker`;
      const authToken = sessionStorage.getItem(SessionStorageEnum.ACCESS_TOKEN);
      if (authToken) {
        url += '?access_token=' + authToken;
      }
      const socket: WebSocket = new SockJS(url);
      this.stompClient = Stomp.over(socket);
      const headers: Stomp.ConnectionHeaders = {};
      this.socketSubscription = this.stompClient.connect(headers, () => {
        observer.next();

        this.stompClient.subscribe('/topic/session/track/' + sessionCode, (data: Stomp.Message) => {
          const session: Session = JSON.parse(data.body);
          this.store.dispatch(new UpdateSession(session));
        });
      }, (error: CloseEvent | Frame) => {
        observer.error(error);
      });
    });
  }

  public joinSession(username: string, sessionCode: string): Observable<void> {
    return new Observable<void>(observer => {
      const url = `${environment.apiRoot}/session/${sessionCode}/join?username=${username}`;
      this.http.get(url).subscribe((res: JoinSessionResponse) => {
        PlayerService.setPlayerToStorate(res.player, res.session.code);
        sessionStorage.setItem(SessionStorageEnum.ACCESS_TOKEN, res.token.id_token);
        this.store.dispatch(new JoinSession(res.session));
        this.router.navigate(['game']);
        observer.next();
      }, (error: HttpErrorResponse) => {
        observer.error(error);
      });
    });
  }

  /**
   * Returns whether a valid session is still in progress
   */
  public isSessionValid(): Observable<boolean> {
    if(this.session) {
      return new Observable(observer => {
        const url = `${environment.apiRoot}/session/${this.session.code}/valid`;

        return this.http.get(url, {responseType: 'text'}).subscribe((isValid: 'true' | 'false') => {
          observer.next(isValid === 'true');
        }, (error) => {
          observer.error(error);
        });
      });
    } else {
      of(false);
    }
  }

}
