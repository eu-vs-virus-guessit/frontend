import { Injectable } from '@angular/core';
import {SessionStorageEnum} from '../enums/SessionStorageEnum';
import {Player} from '../types/game/Player';
import {MiscService} from './misc.service';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  public static getPlayerFromStorage(sessionCode: string): Player {
    return JSON.parse(MiscService.decryptAES(sessionStorage.getItem(SessionStorageEnum.PLAYER), sessionCode));
  }

  public static setPlayerToStorate(player: Player, sessionCode: string): void {
    sessionStorage.setItem(SessionStorageEnum.PLAYER, MiscService.encryptAES(JSON.stringify(player), sessionCode));
  }
}
