import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Observable, of, throwError} from 'rxjs';
import {environment} from '../../../environments/environment';
import {AppState} from '../../app.state';
import {SessionStorageEnum} from '../enums/SessionStorageEnum';
import {LoginUser, LogoutUser} from '../store/actions/auth/auth.actions';
import {EndSession} from '../store/actions/session/session.actions';
import {AuthResponse} from '../types/auth/AuthResponse';
import {AuthenticationRequestBody} from '../types/auth/AuthenticationRequestBody';
import {GameCreatorUser} from '../types/auth/GameCreatorUser';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private readonly store: Store<AppState>,
    private readonly http: HttpClient,
    private readonly router: Router
  ) {
  }

  /**
   * returns whether the user is authenticated or guest
   */
  public isAuthenticated(): Observable<string> {
    if(sessionStorage.getItem(SessionStorageEnum.ACCESS_TOKEN) !== null) {
      const url = `${environment.apiRoot}/authenticate`;

      return this.http.get(url, {responseType: 'text'});
    } else {
      return throwError('');
    }

  }

  /**
   * Logs the current user out
   */
  public logout(): void {
    sessionStorage.removeItem(SessionStorageEnum.ACCESS_TOKEN);
    this.store.dispatch(new LogoutUser());
  }

  /**
   * Logs the current user in
   */
  public login(username: string, password: string): Observable<AuthResponse> {
    return new Observable<AuthResponse>(observer => {
      const url = `${environment.apiRoot}/authenticate`;
      const body: AuthenticationRequestBody = {
        username,
        password,
        rememberMe: false
      };

      this.http.post(url, body).subscribe((auth: AuthResponse) => {
        this.store.dispatch(new LoginUser(auth));
        observer.next(auth);
      }, (error: HttpErrorResponse) => observer.error(error));
    });

  }

  /**
   * Logs the current user in
   */
  public register(username: string, password: string, passwordRepeat: string, email: string): Observable<AuthResponse> {
    const url = `${environment.apiRoot}/register`;
    const user: GameCreatorUser = {
      email,
      login: username,
      password,
      activated: true
    };

    return new Observable<AuthResponse>(observer => {

      this.http.post(url, user).subscribe(() => {
        this.login(username, password).subscribe((auth) => {
          observer.next(auth);
        }, (error) => observer.error(error));
      }, (error: HttpErrorResponse)=> observer.error(error));
    });
  }

  /**
   * Restores the authentication data from sessionStorage
   */
  public restoreAuthentication(): void {
    this.isAuthenticated().subscribe((username: string) => {
      this.store.dispatch(new LoginUser({
        id_token: sessionStorage.getItem(SessionStorageEnum.ACCESS_TOKEN)
      }));
    }, (error: HttpErrorResponse) => {
      sessionStorage.removeItem(SessionStorageEnum.ACCESS_TOKEN);
      sessionStorage.removeItem(SessionStorageEnum.SESSION);
      this.store.dispatch(new EndSession());
      this.router.navigate(['start']);
    });
  }

  /**
   * Authenticate a guest player. Returns 409 Http Status when the username is already taken
   * @param username - the guests desired username
   * @param sessionCode - the sessionCode of the session
   */
  public authenticateGuest(username: string, sessionCode: string): Observable<AuthResponse>  {
    const url = `${environment.apiRoot}/register/${sessionCode}?username=${username}`;

    return this.http.get(url) as Observable<AuthResponse>;
  }
}
