import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {AppState} from '../../app.state';
import {Session} from '../types/session/Session';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private session: Session;

  constructor(
    private router: Router,
    private store: Store<AppState>,
  ) {
    this.store.select('session').subscribe((session: Session) => {
      this.session = session;
    });
  }

  public joinGame(): Observable<string> {
    return new Observable<string>(observer => {
        if (this.session && this.session.game && this.session.players.length < this.session.game.maxPlayers) {
          observer.next();
          this.router.navigate(['game']);
        } else if (this.session.players.length === this.session.game.maxPlayers) {
          observer.error('Lobby is full.');
        } else {
          observer.error('Game id is not valid.');
        }
      });
  }
}
