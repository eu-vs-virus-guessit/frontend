import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import aes from 'crypto-js/aes';
import Base64 from 'crypto-js/enc-base64';
import Utf8 from 'crypto-js/enc-utf8';
import sha256 from 'crypto-js/sha256';
import * as jwtDecode from 'jwt-decode';
import {SessionStorageEnum} from '../enums/SessionStorageEnum';
import {Jwt} from '../types/auth/Jwt';

@Injectable({
  providedIn: 'root'
})
export class MiscService {

  public static encryptSha256(value: string): string {
    const encrypted = CryptoJS.SHA256(value);

    return encrypted.toString(CryptoJS.enc.Base64);
  }

  public static getUsernameFromJwt(): string {
    const token: string = sessionStorage.getItem(SessionStorageEnum.ACCESS_TOKEN);
    if(token) {
      const jwt: Jwt = jwtDecode(sessionStorage.getItem(SessionStorageEnum.ACCESS_TOKEN));

      return jwt.sub;
    }

    return '';
  }

  /**
   * Encrypts a string using the AES algorithm
   * @param ciphertext - the string to encrypt
   * @param passphrase - the passphase to encrypt the string by
   */
  public static encryptAES(ciphertext: string, passphrase: string): string {
    return aes.encrypt(ciphertext, passphrase).toString();
  }

  /**
   * Decrypts a AES encrypted string
   * @param encryptedString - the encrypted string to decrypt
   * @param passphrase - the passphase to decrypt the string by
   */
  public static decryptAES(encryptedString: string, passphrase: string): string {
    const bytes = aes.decrypt(encryptedString, passphrase);

    return bytes.toString(Utf8);
  }
}
