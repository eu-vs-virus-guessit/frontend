import {Injectable} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Store} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {AppState} from '../../app.state';
import {AuthService} from '../services/auth.service';
import {SessionService} from '../services/session.service';
import {Session} from '../types/session/Session';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly store: Store<AppState>
  ) {
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return new Observable<boolean>(observer => {
      this.authService.isAuthenticated().subscribe((username: string) => {
        console.log('AuthGuard isAuthenticated', !!username);
        if(!username) {
          observer.next(false);
          this.router.navigate(
            ['login'],
            {
              queryParams: {
                returnUrl: this.router.url
              }
            });
        } else {
          observer.next(!!username);
        }
      });
    });
  }
}
