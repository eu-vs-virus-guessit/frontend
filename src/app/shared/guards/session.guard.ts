import {HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {is} from 'uribuilder';
import {SessionService} from '../services/session.service';
import {Session} from '../types/session/Session';

@Injectable()
export class SessionGuard implements CanActivate {
  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly sessionService: SessionService
  ) {
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return new Observable<boolean>(observer => {
      this.sessionService.isSessionValid().subscribe((isValid: boolean) => {
        observer.next(isValid);
      }, (error: HttpErrorResponse) => {
        console.error(error);
        observer.next(false);
      });
    });

  }
}
