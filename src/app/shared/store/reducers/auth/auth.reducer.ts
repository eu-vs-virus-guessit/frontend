import {SessionStorageEnum} from '../../../enums/SessionStorageEnum';
import {MiscService} from '../../../services/misc.service';
import {AuthResponse} from '../../../types/auth/AuthResponse';
import * as AuthActions from '../../actions/auth/auth.actions.js';

export function authReducer(state: AuthResponse, action: AuthActions.Actions) {
  switch(action.type) {
    case AuthActions.LOGIN:
      sessionStorage.setItem(SessionStorageEnum.ACCESS_TOKEN, action.payload.id_token);
      return action.payload;
    case AuthActions.LOGOUT:
      sessionStorage.removeItem(SessionStorageEnum.ACCESS_TOKEN);
      return null;
    default:
      return state;
  }
}
