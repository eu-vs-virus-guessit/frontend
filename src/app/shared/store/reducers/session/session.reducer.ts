import {SessionStateEnum} from '../../../enums/SessionStateEnum';
import {SessionStorageEnum} from '../../../enums/SessionStorageEnum';
import {Session} from '../../../types/session/Session';
import * as SessionActions from '../../actions/session/session.actions.js';

export function sessionReducer(state: Session = null, action: SessionActions.Actions) {
  switch (action.type) {
    case SessionActions.SESSION_JOIN:
      sessionStorage.setItem(SessionStorageEnum.SESSION, action.payload.code);
      return action.payload;
    case SessionActions.SESSION_LEAVE:
      sessionStorage.removeItem(SessionStorageEnum.SESSION);
      return null;
    case SessionActions.SESSION_UPDATE:
      return action.payload;
    case SessionActions.SESSION_END:
      return null;
    default:
      return state;
  }
}
