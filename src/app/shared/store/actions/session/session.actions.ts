import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Action} from '@ngrx/store';
import {AuthResponse} from '../../../types/auth/AuthResponse';
import {Session} from '../../../types/session/Session';

export const SESSION_JOIN = '[SESSION] join';
export const SESSION_UPDATE = '[SESSION] update';
export const SESSION_LEAVE = '[SESSION] leave';
export const SESSION_END = '[SESSION] end';

export class JoinSession implements Action {
  public readonly type = SESSION_JOIN;

  constructor(
    public payload: Session) {
  }
}

export class LeaveSession implements Action {
  public readonly type = SESSION_LEAVE;
}

export class UpdateSession implements Action {
  public readonly type = SESSION_UPDATE;

  constructor(
    public payload: Session) {
  }
}

export class EndSession implements Action {
  public readonly type = SESSION_END;
}

export type Actions = JoinSession | LeaveSession | EndSession | UpdateSession;
