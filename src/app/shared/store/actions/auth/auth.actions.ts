import {Action} from '@ngrx/store';
import {AuthResponse} from '../../../types/auth/AuthResponse';

export const LOGIN = '[AUTH] Login';
export const LOGOUT = '[AUTH] Logout';

export class LoginUser implements Action {
  public readonly type = LOGIN;

  constructor(public payload: AuthResponse) {
  }
}

export class LogoutUser implements Action {
  public readonly type = LOGOUT;
}

export type Actions = LoginUser | LogoutUser;
