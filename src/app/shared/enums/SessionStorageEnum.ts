export enum SessionStorageEnum {
  SESSION = 'session_id',
  ACCESS_TOKEN = 'access-token',
  PLAYER = 'player'
}
