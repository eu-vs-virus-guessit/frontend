export enum GameRoleEnum {
  GUESSER = 'GUESSER',
  THINKER = 'THINKER'
}
