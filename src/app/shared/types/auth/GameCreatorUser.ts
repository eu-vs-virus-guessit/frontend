export interface GameCreatorUser {
  activated?: boolean;
  authorities?: string[];
  createdBy?: string;
  createdDate?: string;
  email: string;
  firstName?: string;
  id?: string;
  imageUrl?: string;
  langKey?: string;
  lastModifiedBy?: string;
  lastModifiedDate?: string;
  lastName?: string;
  login: string;
  password: string;
}
