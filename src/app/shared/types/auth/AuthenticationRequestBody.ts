export interface AuthenticationRequestBody {
  password: string;
  username: string;
  rememberMe: boolean;
}
