export interface Jwt {
  /**
   * Username
   */
  sub: string;

  /**
   * Role
   */
  auth: string;

  /**
   * Expiration date as Unix timestamp
   */
  exp: number;
}
