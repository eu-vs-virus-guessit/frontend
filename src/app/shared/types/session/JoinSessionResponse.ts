import {AuthResponse} from '../auth/AuthResponse';
import {Player} from '../game/Player';
import {Session} from './Session';

export interface JoinSessionResponse {
  session: Session;
  token: AuthResponse;
  player: Player;
}
