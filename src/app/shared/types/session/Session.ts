import {SessionStateEnum} from '../../enums/SessionStateEnum';
import {Game} from '../game/Game';
import {Player} from '../game/Player';

export interface Session {
  id?: number;
  code: string;
  name: string;
  state: SessionStateEnum;
  players: Player[];
  dateCreated: string | number;
  dateModified: string | number;
  game?: Game;
}
