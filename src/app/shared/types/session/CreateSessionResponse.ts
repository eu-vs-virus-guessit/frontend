import {Player} from '../game/Player';
import {Session} from './Session';

export interface CreateSessionResponse {
  session: Session;
  player: Player;
}
