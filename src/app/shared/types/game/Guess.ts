import {Player} from './Player';

export interface Guess {
  content: string;
  isValid: string;
  player: Player;
}
