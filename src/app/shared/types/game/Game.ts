import {GameStateEnum} from '../../enums/GameStateEnum';
import {Language} from './Language';
import {Round} from './Round';

export interface Game {
  maxPlayers: number;
  language: Language;
  state: GameStateEnum;
  rounds: Round[];
  dateCreated: string;
  dateModified: string;
}
