import {Guess} from './Guess';

export interface Round {
  iteration: number;
  dateCreated: string;
  dateModified: string;
  guesses: Guess[];
}
