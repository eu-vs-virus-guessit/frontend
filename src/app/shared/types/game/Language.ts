export interface Language {
  name: string;
  iso6391: string;
}
