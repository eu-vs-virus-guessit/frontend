export interface Player {
  name: string;
  admin: boolean;
  dateCreated: string;
  dateModified: string;
}

export function createPlayer(): Player {
  return {
    name: '',
    admin: true,
    dateCreated: '',
    dateModified: ''
  };
}
