import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {MatSlider, MatSliderModule} from '@angular/material/slider';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatSliderModule
  ],
  exports: [
    MatSliderModule
  ]
})
export class ThirdPartyModule { }
