import * as momentLib from 'moment';
momentLib.locale('de');

// make moment libary easily accessible from everywhere.
export const moment = momentLib;
