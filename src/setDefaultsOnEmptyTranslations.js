const fs = require('fs');
const path = require('path');

const config = {
  dir: './src/assets/i18n', // <-- your json translations directory
  defaultLang: 'en'
};

fs.readdirSync(config.dir).forEach(file => {
  if (path.extname(file) === '.json') {
    const filePath = `${config.dir}/${file}`;
    const json = fs.readFileSync(filePath, 'utf-8');
    const obj = JSON.parse(json);
    if(path.basename(file) === config.defaultLang + '.json'){
      Object.keys(obj).forEach(key => {
        if (obj[key] === '') {
          obj[key] = key;
        }
      });
    } else {
      Object.keys(obj).forEach(key => {
        if (obj[key] === '') {
          obj[key] = '_[DEFAULT_VALUE]_' + key;
        }
      });
    }
    const str = JSON.stringify(obj, null, '\t');
    fs.writeFileSync(filePath, str, 'utf-8');
    console.log('Defaults set for ' + path.basename(file))
  }
});
